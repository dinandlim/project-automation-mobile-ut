import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('First Loan/Start Apps'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Home Page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Loan Calculator'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 1 Pengajuan Pinjaman'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 2 PHA dan Alasan Pinjam'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 3 Data Diri'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 4 Pendidikan Terakhir'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 5 Suku dan Jumlah Tanggungan'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 6 No Telp Rumah'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 7 Alamat Tempat Tinggal'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 8 Status Rumah'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 9 Data Keluarga'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 10 Nama dan Bank Account'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 11 Data Kantor'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step 13 Upload KTP dan Foto Alternatif'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Step Dokumen Persetujuan'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Persetujuan Permohonan'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Berkas Permohonan Diterima'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('First Loan/Darimana Tahu UT'), [:], FailureHandling.STOP_ON_FAILURE)

