import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.time.LocalDate
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

Integer TextBulanDebitur

Integer TextBulanHrIni

Integer Usia

DBData data = findTestData('Step Dokumen Persetujuan')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Debitur_DB = data.getValue(1, index)

    Tgl_Lahir_Debitur_DB = data.getValue(2, index)

    Tempat_Lahir_Debitur_DB = data.getValue(3, index)

    NoHp_Debitur_DB = data.getValue(4, index)

    TelpRumah_Debitur_DB = data.getValue(5, index)

    Email_Debitur_DB = data.getValue(6, index)

    Bank_Debitur_DB = data.getValue(7, index)

    Rekening_Debitur_DB = data.getValue(8, index)

    KTP_Debitur_DB = data.getValue(9, index)

    Alamat_Debitur_DB = data.getValue(10, index)
	
	Kota_Debitur_DB = data.getValue(11, index)
	
	Kecamatan_Debitur_DB = data.getValue(12, index)
	
	Kelurahan_Debitur_DB = data.getValue(13, index)
	
	Provinsi_Debitur_DB = data.getValue(14, index)
	
	
	//Tanggal Lahir Debitur
	String tanggal_lahir = Tgl_Lahir_Debitur_DB
	
	String[] split = tanggal_lahir.split('-')
	
	String TanggalDebitur = split[0]
	
	String BulanDebitur = split[1]
	
	String TahunDebitur = split[2]
	
	if(BulanDebitur.equals("Jan")){
		TextBulanDebitur = '1'
	}
	else if(BulanDebitur.equals("Feb")){
		TextBulanDebitur = '2'
	}
	else if(BulanDebitur.equals("Mar")){
		TextBulanDebitur = '3'
	}
	else if(BulanDebitur.equals("Apr")){
		TextBulanDebitur = '4'
	}
	else if(BulanDebitur.equals("May")){
		TextBulanDebitur = '5'
	}
	else if(BulanDebitur.equals("Jun")){
		TextBulanDebitur = '6'
	}
	else if(BulanDebitur.equals("Jul")){
		TextBulanDebitur = '7'
	}
	else if(BulanDebitur.equals("Aug")){
		TextBulanDebitur = '8'
	}
	else if(BulanDebitur.equals("Sep")){
		TextBulanDebitur = '9'
	}
	else if(BulanDebitur.equals("Oct")){
		TextBulanDebitur = '10'
	}
	else if(BulanDebitur.equals("Nov")){
		TextBulanDebitur = '11'
	}
	else if(BulanDebitur.equals("Dec")){
		TextBulanDebitur = '12'
	}
	
	//Tanggal Hari Ini
	Format formatter = new SimpleDateFormat('dd MMM yyyy')
	
	String TodayDate = formatter.format(new Date())
	
	String[] split2 = TodayDate.split(' ')
	
	String TanggalHrIni = split2[0]
	
	String BulanHrIni = split2[1]
	
	String TahunHrIni = split2[2]
	
	if(BulanHrIni.equals("Jan")){
		TextBulanHrIni = '1'
	}
	else if(BulanHrIni.equals("Feb")){
		TextBulanHrIni = '2'
	}
	else if(BulanHrIni.equals("Mar")){
		TextBulanHrIni = '3'
	}
	else if(BulanHrIni.equals("Apr")){
		TextBulanHrIni = '4'
	}
	else if(BulanHrIni.equals("May")){
		TextBulanHrIni = '5'
	}
	else if(BulanHrIni.equals("Jun")){
		TextBulanHrIni = '6'
	}
	else if(BulanHrIni.equals("Jul")){
		TextBulanHrIni = '7'
	}
	else if(BulanHrIni.equals("Aug")){
		TextBulanHrIni = '8'
	}
	else if(BulanHrIni.equals("Sep")){
		TextBulanHrIni = '9'
	}
	else if(BulanHrIni.equals("Oct")){
		TextBulanHrIni = '10'
	}
	else if(BulanHrIni.equals("Nov")){
		TextBulanHrIni = '11'
	}
	else if(BulanHrIni.equals("Dec")){
		TextBulanHrIni = '12'
	}
	
	Integer intTanggalDebitur = Integer.parseInt(TanggalDebitur)
	
	Integer intTahunDebitur = Integer.parseInt(TahunDebitur)
	
	Integer intTanggalHrIni = Integer.parseInt(TanggalHrIni)
	
	Integer intTahunHrIni = Integer.parseInt(TahunHrIni)
	
	Integer TahunUmur = (intTahunHrIni - intTahunDebitur) - 1
	
	if(TextBulanHrIni > TextBulanDebitur){
		Usia = TahunUmur +1
	}
	else if(TextBulanHrIni == TextBulanDebitur){
		if(intTanggalHrIni >= intTanggalDebitur){
			Usia = TahunUmur + 1
		}
		else{
			Usia = TahunUmur
		}
	}
	else{
		Usia = TahunUmur
	}
	
	String VerifikasiUsia = Usia +" Tahun"
	
	print(Usia)
	
}




