import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 7 Alamat Tempat Tinggal')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Alamat_Tinggal_DB = data.getValue(1, index)

    Provinsi_Tinggal_DB = data.getValue(2, index)

    Kota_Tinggal_DB = data.getValue(3, index)

    Kecamatan_Tinggal_DB = data.getValue(4, index)

    Kelurahan_Tinggal_DB = data.getValue(5, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Textfield_Alamat_Tinggal'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Textfield_Alamat_Tinggal'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Provinsi_Tinggal'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kota_Tinggal'), 0)

    Mobile.scrollToText('Kode Pos Tempat Tinggal', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kecamatan_Tinggal'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kelurahan_Tinggal'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Text_KodePos_Tempat_Tinggal'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Text_KodePos_Tempat_Tinggal'), 0)

    Mobile.scrollToText('Alamat Tempat Tinggal', FailureHandling.STOP_ON_FAILURE)

    //Alamat Tempat Tinggal
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Textfield_Alamat_Tinggal'), 0)

    Mobile.setText(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Textfield_Alamat_Tinggal'), Alamat_Tinggal_DB, 
        0)

    //Provinsi Tempat Tinggal
    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Provinsi_Tinggal'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/List_Bali'), 0)

    Mobile.scrollToText(Provinsi_Tinggal_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Provinsi', [('List_Provinsi') : Provinsi_Tinggal_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Provinsi', [('List_Provinsi') : Provinsi_Tinggal_DB]), 
        0)

    //Kota Tempat Tinggal
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kota_Tinggal'), 0)

    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kota_Tinggal'), 0)

    Mobile.scrollToText(Kota_Tinggal_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Kota', [('List_Kota') : Kota_Tinggal_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Kota', [('List_Kota') : Kota_Tinggal_DB]), 
        0)

    //Kecamatan Tempat Tinggal
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kecamatan_Tinggal'), 0)

    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kecamatan_Tinggal'), 0)

    Mobile.scrollToText(Kecamatan_Tinggal_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Kecamatan', [('List_Kecamatan') : Kecamatan_Tinggal_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Kecamatan', [('List_Kecamatan') : Kecamatan_Tinggal_DB]), 
        0)

    Mobile.scrollToText('Kode Pos Tempat Tinggal', FailureHandling.STOP_ON_FAILURE)

    //Kelurahan Tempat Tinggal
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kelurahan_Tinggal'), 0)

    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Kelurahan_Tinggal'), 0)

    Mobile.scrollToText(Kelurahan_Tinggal_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Kelurahan', [('List_Kelurahan') : Kelurahan_Tinggal_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Pilih_List_Kelurahan', [('List_Kelurahan') : Kelurahan_Tinggal_DB]), 
        0)
	
	Mobile.scrollToText('Kode Pos Tempat Tinggal', FailureHandling.STOP_ON_FAILURE)
	
	Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Text_KodePos_Tempat_Tinggal'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Text_KodePos_Tempat_Tinggal'), 0)

	GlobalVariable.txt_kode_pos = Mobile.getText(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Text_KodePos_Tempat_Tinggal'),
		0)
}

Mobile.waitForElementPresent(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Selanjutnya'), 0)

Mobile.tap(findTestObject('First Loan/Step 7 Alamat Tempat Tinggal/Btn_Selanjutnya'), 0)

