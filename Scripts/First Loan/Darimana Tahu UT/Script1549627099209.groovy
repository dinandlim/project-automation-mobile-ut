import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Darimana Tahu UT')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    DarimanaTahuUT = data.getValue(1, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Darimana Tahu UT/List_Google'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Google'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Facebook'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Twitter'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Email'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Radio'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Ref_Teman'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Billboard'), 0)

    Mobile.scrollToText('Bioskop', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Others'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_SMS'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Instagram'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Brosur'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Bioskop'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Iklan_Banner_Mobile'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Darimana Tahu UT/List_Iklan_Banner_Website'), 0)

    Mobile.scrollToText(DarimanaTahuUT, FailureHandling.STOP_ON_FAILURE)

    //Mobile.waitForElementPresent(findTestObject('First Loan/Darimana Tahu UT/List_Choosen', [('List_DarimanaTahu') : DarimanaTahuUT]), 0, FailureHandling.STOP_ON_FAILURE)

    //Mobile.tap(findTestObject('First Loan/Darimana Tahu UT/List_Choosen', [('List_DarimanaTahu') : DarimanaTahuUT]), 0) 
	
	boolean verifyDarimanaTahuUT = Mobile.waitForElementPresent(findTestObject('First Loan/Darimana Tahu UT/List_Choosen', [('List_DarimanaTahu') : DarimanaTahuUT]),0 ,FailureHandling.OPTIONAL)
    
	if (verifyDarimanaTahuUT == true) {
		Mobile.waitForElementPresent(findTestObject('First Loan/Darimana Tahu UT/List_Choosen', [('List_DarimanaTahu') : DarimanaTahuUT]), 0 ,FailureHandling.STOP_ON_FAILURE)
		
		Mobile.tap(findTestObject('First Loan/Darimana Tahu UT/List_Choosen', [('List_DarimanaTahu') : DarimanaTahuUT]),   0)
    } else {
      	Mobile.scrollToText(DarimanaTahuUT, FailureHandling.STOP_ON_FAILURE)
		
		boolean verifyDarimanaTahuUT2 = Mobile.waitForElementPresent(findTestObject('First Loan/Darimana Tahu UT/List_Choosen', [('List_DarimanaTahu') : DarimanaTahuUT]), 0,FailureHandling.OPTIONAL)
		
		if (verifyDarimanaTahuUT2 == true) {
			Mobile.tap(findTestObject('First Loan/Darimana Tahu UT/List_Choosen', [('List_DarimanaTahu') : DarimanaTahuUT]), 0)
		} else {
			CustomKeywords.'set.testing.markFailedAndStop'('Text not Valid.. Please Check DB')
		}
    }
}

Mobile.tap(findTestObject('First Loan/Darimana Tahu UT/Btn_Kirim'), 0)

