import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String textSuku

DBData data = findTestData('First Loan/Step 5 Suku dan Jumlah Tanggungan')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    SukuDB = data.getValue(1, index)

    Jumlah_TanggunganDB = data.getValue(2, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/Btn_Pilih_Suku'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/Btn_Pilih_Suku'), 0)

    Mobile.tap(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/Btn_Pilih_Suku'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Jawa'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Jawa'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Cina'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Sunda'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Melayu'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Madura'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Batak'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Minangkabau'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Betawi'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Bugis'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Banten'), 0)

    Mobile.scrollToText('Lainnya', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/List_Lainnya'), 0)

    //Pilih Suku
    if (SukuDB.equals('1')) {
        textSuku = 'Jawa'
    } else if (SukuDB.equals('2')) {
        textSuku = 'Cina'
    } else if (SukuDB.equals('3')) {
        textSuku = 'Sunda'
    } else if (SukuDB.equals('4')) {
        textSuku = 'Melayu'
    } else if (SukuDB.equals('5')) {
        textSuku = 'Madura'
    } else if (SukuDB.equals('6')) {
        textSuku = 'Batak'
    } else if (SukuDB.equals('7')) {
        textSuku = 'Minangkabau'
    } else if (SukuDB.equals('8')) {
        textSuku = 'Betawi'
    } else if (SukuDB.equals('9')) {
        textSuku = 'Bugis'
    } else if (SukuDB.equals('10')) {
        textSuku = 'Banten'
    } else if (SukuDB.equals('11')) {
        textSuku = 'Lainnya'
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Index not Valid.. Please Check DB')
    }
    
    if (SukuDB < 2) {
        Mobile.scrollToText(textSuku, FailureHandling.STOP_ON_FAILURE)
    }
    
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/Pilih_List_Suku', [('index_Suku') : textSuku]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/List Suku/Pilih_List_Suku', [('index_Suku') : textSuku]), 
        0)

    //Menentukan Jumlah Tanggungan
    TanggunganSaatIni = Mobile.getText(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/Text_Jumlah_Tanggungan'), 0)

    if (TanggunganSaatIni < Jumlah_TanggunganDB) {
        for (int a = TanggunganSaatIni; a < Jumlah_TanggunganDB; a++) {
            Mobile.tap(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/Btn_Tambah_Tanggungan'), 0)
        }
    } else if (TanggunganSaatIni > Jumlah_TanggunganDB) {
        for (int a = TanggunganSaatIni; a > Jumlah_TanggunganDB; a--) {
            Mobile.tap(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/Btn_Kurang_Tanggungan'), 0)
        }
    } else if (TanggunganSaatIni == Jumlah_TanggunganDB) {
        CustomKeywords.'set.MarkAndMessage.markPassed'('Tanggungan saat ini sudah sama dengan permintaan di DB')
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Index not Valid.. Please Check DB')
    }
}

Mobile.tap(findTestObject('First Loan/Step 5 Suku dan Jumlah Tanggungan/Btn_Selanjutnya'), 0)

