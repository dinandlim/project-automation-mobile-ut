import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('First Loan/Home Page/Btn_Lain_Kali'), 0)

Mobile.tap(findTestObject('First Loan/Home Page/Btn_Lain_Kali'), 0)

Boolean Dashboard_awal = Mobile.verifyElementVisible(findTestObject('First Loan/Home Page/Btn_Lewati'), 0, FailureHandling.OPTIONAL)

if (Dashboard_awal == true) {
    Mobile.waitForElementPresent(findTestObject('First Loan/Home Page/Btn_Lewati'), 0)

    Mobile.tap(findTestObject('First Loan/Home Page/Btn_Lewati'), 0)
	
	Mobile.waitForElementPresent(findTestObject('First Loan/Home Page/Text_Pinjaman_Online'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Home Page/Text_Pinjaman_Online'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Home Page/Btn_Menu'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Home Page/Btn_Ajukan_Pinjaman'), 0)
	
	Mobile.tap(findTestObject('First Loan/Home Page/Btn_Ajukan_Pinjaman'), 0)
}
else{
	Mobile.waitForElementPresent(findTestObject('First Loan/Home Page/Text_Pinjaman_Online'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Home Page/Text_Pinjaman_Online'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Home Page/Btn_Menu'), 0)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Home Page/Btn_Ajukan_Pinjaman'), 0)
	
	Mobile.tap(findTestObject('First Loan/Home Page/Btn_Ajukan_Pinjaman'), 0)
}



