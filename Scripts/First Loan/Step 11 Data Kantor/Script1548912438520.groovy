import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 11 Data Kantor')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Kantor_DB = data.getValue(1, index)

    NoTelp_Kantor_DB = data.getValue(2, index)

    Alamat_Kantor_DB = data.getValue(3, index)

    Provinsi_Kantor_DB = data.getValue(4, index)

    Kota_Kantor_DB = data.getValue(5, index)

    Kecamatan_Kantor_DB = data.getValue(6, index)

    Kelurahan_Kantor_DB = data.getValue(7, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Textfield_Nama_Kantor'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 11 Data Kantor/Textfield_Nama_Kantor'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 11 Data Kantor/Textfield_NoTelp_Kantor'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 11 Data Kantor/Textfield_Alamat_Kantor'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 11 Data Kantor/Btn_Provinsi_Kantor'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kota_Kantor'), 0)

    Mobile.scrollToText('Kelurahan Kantor', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kecamatan_Kantor'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kelurahan_Kantor'), 0)

    Mobile.scrollToText('Nama Kantor', FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('First Loan/Step 11 Data Kantor/Textfield_Nama_Kantor'), Nama_Kantor_DB, 0)

    Mobile.hideKeyboard()

    Mobile.setText(findTestObject('First Loan/Step 11 Data Kantor/Textfield_NoTelp_Kantor'), NoTelp_Kantor_DB, 0)

    Mobile.hideKeyboard()

    Mobile.setText(findTestObject('First Loan/Step 11 Data Kantor/Textfield_Alamat_Kantor'), Alamat_Kantor_DB, 0)

    Mobile.hideKeyboard()

    //Pilih Provinsi
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Btn_Provinsi_Kantor'), 0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Btn_Provinsi_Kantor'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/List_Provinsi_Kantor'), 0)

    Mobile.scrollToText(Provinsi_Kantor_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Provinsi_Kantor', [('Pilih_List_Provinsi_Kantor') : Provinsi_Kantor_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Provinsi_Kantor', [('Pilih_List_Provinsi_Kantor') : Provinsi_Kantor_DB]), 
        0)

    //Pilih Kota
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kota_Kantor'), 0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kota_Kantor'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/List_Kota_Kantor'), 0)

    Mobile.scrollToText(Kota_Kantor_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Kota_Kantor', [('Pilih_List_Kota_Kantor') : Kota_Kantor_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Kota_Kantor', [('Pilih_List_Kota_Kantor') : Kota_Kantor_DB]), 
        0)

    //Pilih Kecamatan
    Mobile.scrollToText('Kelurahan Kantor', FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kecamatan_Kantor'), 0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kecamatan_Kantor'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/List_Kecamatan_Kantor'), 0)

    Mobile.scrollToText(Kecamatan_Kantor_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Kecamatan_Kantor', [('Pilih_List_Kecamatan_Kantor') : Kecamatan_Kantor_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Kecamatan_Kantor', [('Pilih_List_Kecamatan_Kantor') : Kecamatan_Kantor_DB]), 
        0)

    //Pilih Kelurahan
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kelurahan_Kantor'), 0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Btn_Kelurahan_Kantor'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/List_Kelurahan_Kantor'), 0)

    Mobile.scrollToText(Kelurahan_Kantor_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Kelurahan_Kantor', [('Pilih_List_Kelurahan_Kantor') : Kelurahan_Kantor_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Pilih_List_Kelurahan_Kantor', [('Pilih_List_Kelurahan_Kantor') : Kelurahan_Kantor_DB]), 
        0)
}

Mobile.waitForElementPresent(findTestObject('First Loan/Step 11 Data Kantor/Btn_Selanjutnya'), 0)

Mobile.tap(findTestObject('First Loan/Step 11 Data Kantor/Btn_Selanjutnya'), 0)

