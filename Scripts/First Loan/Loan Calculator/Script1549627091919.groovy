import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.MobileBy as MobileBy
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import io.appium.java_client.android.AndroidElement as AndroidElement
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.remote.CapabilityType as CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities

DBData data = findTestData('First Loan/Loan Calculator Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

Integer percentSlider = 0

Integer percentSlider_Tenor = 0

for (def index : (1..data.getRowNumbers())) {
    Jumlah_Pinjaman = data.getValue(1, index)

    Durasi_Pinjaman = data.getValue(2, index)

    Integer int_JumlahPinjaman = Integer.parseInt(Jumlah_Pinjaman)

    Integer int_JumlahDurasi = Integer.parseInt(Durasi_Pinjaman)

    boolean IkutiSurvey = Mobile.verifyElementVisible(findTestObject('First Loan/Ikuti Survey Box/Btn_Ikut_Survey'), 0, 
        FailureHandling.OPTIONAL)

    if (IkutiSurvey == true) {
        Mobile.pressBack()
    }
    
    Mobile.waitForElementPresent(findTestObject('First Loan/Loan Calculator/Btn_Pinjaman_Sekarang'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Loan Calculator/Seekbar_Jumlah_Pinjaman'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Loan Calculator/Text_Jumlah_Pinjaman'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Loan Calculator/Text_Durasi_Pinjaman'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Loan Calculator/Text_Biaya_Layanan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Loan Calculator/Text_Jumlah_Pembayaran'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Loan Calculator/Text_Jatuh_Tempo'), 0)

    //if (Jumlah_Pinjaman >= 1000000) {
    //Integer TotalDrag = ((int_JumlahPinjaman - 1000000 )/ 100000)*20
    //Integer TotalDragJumlahPinjaman = (int_JumlahPinjaman - 1000000) / 100000
    //def SliderJumlahPinjaman = print(TotalDragJumlahPinjaman +".0")
    //SliderJumlahPinjaman
    //TestObject lblJumlah_Pinjaman = new TestObject('')
    //lblJumlah_Pinjaman.addProperty('xpath', ConditionType.EQUALS, '//*[@class = \'android.widget.SeekBar\' and (text() = \'15.0\' or . = \'15.0\') and @resource-id = \'com.dai.uangteman.dev:id/loan_amount_seekbar\']') //lblJumlah_Pinjaman = driver.findElement(By.xpath('//*[@class = \'android.widget.SeekBar\' and (text() = \'15.0\' or . = \'15.0\') and @resource-id = \'com.dai.uangteman.dev:id/loan_amount_seekbar\']'))
    if (Jumlah_Pinjaman.equals('1100000')) {
        percentSlider = 0
    } else if (Jumlah_Pinjaman.equals('1200000')) {
        percentSlider = 7
    } else if (Jumlah_Pinjaman.equals('1300000')) {
        percentSlider = 13
    } else if (Jumlah_Pinjaman.equals('1400000')) {
        percentSlider = 19
    } else if (Jumlah_Pinjaman.equals('1500000')) {
        percentSlider = 24
    } else if (Jumlah_Pinjaman.equals('1600000')) {
        percentSlider = 30
    } else if (Jumlah_Pinjaman.equals('1700000')) {
        percentSlider = 35
    } else if (Jumlah_Pinjaman.equals('1800000')) {
        percentSlider = 41
    } else if (Jumlah_Pinjaman.equals('1900000')) {
        percentSlider = 47
    } else if (Jumlah_Pinjaman.equals('2000000')) {
        percentSlider = 52
    } else if (Jumlah_Pinjaman.equals('2100000')) {
        percentSlider = 58
    } else if (Jumlah_Pinjaman.equals('2200000')) {
        percentSlider = 64
    } else if (Jumlah_Pinjaman.equals('2300000')) {
        percentSlider = 69
    } else if (Jumlah_Pinjaman.equals('2400000')) {
        percentSlider = 75
    } else if (Jumlah_Pinjaman.equals('2500000')) {
        percentSlider = 80
    } else if (Jumlah_Pinjaman.equals('2600000')) {
        percentSlider = 86
    } else if (Jumlah_Pinjaman.equals('2700000')) {
        percentSlider = 92
    } else if (Jumlah_Pinjaman.equals('2800000')) {
        percentSlider = 97
    } else if (Jumlah_Pinjaman.equals('2900000')) {
        percentSlider = 100
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Jumlah Pinjaman diluar limit')
    }
    
    Mobile.setSliderValue(findTestObject('First Loan/Loan Calculator/Seekbar_Jumlah_Pinjaman'), percentSlider, 0, FailureHandling.OPTIONAL)

    if (Durasi_Pinjaman.equals('11')) {
        percentSlider_Tenor = 0
    } else if (Durasi_Pinjaman.equals('12')) {
        percentSlider_Tenor = 7
    } else if (Durasi_Pinjaman.equals('13')) {
        percentSlider_Tenor = 13
    } else if (Durasi_Pinjaman.equals('14')) {
        percentSlider_Tenor = 19
    } else if (Durasi_Pinjaman.equals('15')) {
        percentSlider_Tenor = 24
    } else if (Durasi_Pinjaman.equals('16')) {
        percentSlider_Tenor = 30
    } else if (Durasi_Pinjaman.equals('17')) {
        percentSlider_Tenor = 35
    } else if (Durasi_Pinjaman.equals('18')) {
        percentSlider_Tenor = 41
    } else if (Durasi_Pinjaman.equals('19')) {
        percentSlider_Tenor = 47
    } else if (Durasi_Pinjaman.equals('20')) {
        percentSlider_Tenor = 52
    } else if (Durasi_Pinjaman.equals('21')) {
        percentSlider_Tenor = 58
    } else if (Durasi_Pinjaman.equals('22')) {
        percentSlider_Tenor = 64
    } else if (Durasi_Pinjaman.equals('23')) {
        percentSlider_Tenor = 69
    } else if (Durasi_Pinjaman.equals('24')) {
        percentSlider_Tenor = 75
    } else if (Durasi_Pinjaman.equals('25')) {
        percentSlider_Tenor = 80
    } else if (Durasi_Pinjaman.equals('26')) {
        percentSlider_Tenor = 86
    } else if (Durasi_Pinjaman.equals('27')) {
        percentSlider_Tenor = 92
    } else if (Durasi_Pinjaman.equals('28')) {
        percentSlider_Tenor = 97
    } else if (Durasi_Pinjaman.equals('29')) {
        percentSlider_Tenor = 100
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Durasi Pinjaman diluar limit')
    }
    
    Mobile.setSliderValue(findTestObject('First Loan/Loan Calculator/Seekbar_Durasi_Pinjaman'), percentSlider_Tenor, 
        0, FailureHandling.OPTIONAL)
}

GlobalVariable.txt_jumlah_pinjaman = Mobile.getText(findTestObject('First Loan/Loan Calculator/Text_Jumlah_Pinjaman'), 
    0)

GlobalVariable.txt_durasi_pinjaman = Mobile.getText(findTestObject('First Loan/Loan Calculator/Text_Durasi_Pinjaman'), 
    0)

GlobalVariable.txt_biaya_layanan = Mobile.getText(findTestObject('First Loan/Loan Calculator/Text_Biaya_Layanan'), 
    0)

GlobalVariable.txt_jumlah_pembayaran = Mobile.getText(findTestObject('First Loan/Loan Calculator/Text_Jumlah_Pembayaran'), 
    0)

GlobalVariable.txt_jatuh_tempo = Mobile.getText(findTestObject('First Loan/Loan Calculator/Text_Jatuh_Tempo'), 0)

Mobile.tap(findTestObject('First Loan/Loan Calculator/Btn_Pinjaman_Sekarang'), 0, FailureHandling.STOP_ON_FAILURE)

