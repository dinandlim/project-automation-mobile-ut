import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 2 Alasan Pinjam Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

String text_AlasanPinjam

for (def index : (1..data.getRowNumbers())) {
    Alasan_Pinjam = data.getValue(1, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Pendidikan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Pendidikan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Pembelian_Konsumen'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Membayar_Hutang'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Liburan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Lain-lain'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Modal_Usaha'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Tagihan_Medis'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Pembelian_Motor'), 0)

    Mobile.scrollToText('Biaya Menikah', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Pembelian_Kendaraan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Biaya_Menikah'), 0)

    if (Alasan_Pinjam.equals('1')) {
        text_AlasanPinjam = 'Pendidikan'
    } else if (Alasan_Pinjam.equals('2')) {
        text_AlasanPinjam = 'Pembelian Konsumen'
    } else if (Alasan_Pinjam.equals('3')) {
        text_AlasanPinjam = 'Membayar Hutang'
    } else if (Alasan_Pinjam.equals('4')) {
        text_AlasanPinjam = 'Liburan'
    } else if (Alasan_Pinjam.equals('5')) {
        text_AlasanPinjam = 'Lain-lain'
    } else if (Alasan_Pinjam.equals('6')) {
        text_AlasanPinjam = 'Modal Usaha'
    } else if (Alasan_Pinjam.equals('7')) {
        text_AlasanPinjam = 'Tagihan Medis '
    } else if (Alasan_Pinjam.equals('8')) {
        text_AlasanPinjam = 'Pembelian Motor'
    } else if (Alasan_Pinjam.equals('9')) {
        text_AlasanPinjam = 'Pembelian Kendaraan'
    } else if (Alasan_Pinjam.equals('10')) {
        text_AlasanPinjam = 'Biaya Menikah'
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Pilihan Alasan Pinjam diluar index yang ditentukan. please check database')
    }
    
    Integer int_Alasan_Pinjam = Integer.parseInt(Alasan_Pinjam)

    if (int_Alasan_Pinjam < 3) {
        Mobile.scrollToText('Pendidikan', FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Alasan_Pinjam', [('index_Alasan_Pinjam') : text_AlasanPinjam]), 
            0)
    } else {
        Mobile.tap(findTestObject('First Loan/Step 2 Alasan Pinjam/List_Alasan_Pinjam', [('index_Alasan_Pinjam') : text_AlasanPinjam]), 
            0)
    }
}

Mobile.tap(findTestObject('First Loan/Step 2 Alasan Pinjam/Btn_Selanjutnya'), 0)

