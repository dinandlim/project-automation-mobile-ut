import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 10 Nama dan Bank Account')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Pemilik_Akun_DB = data.getValue(1, index)

    Nama_Bank_DB = data.getValue(2, index)

    No_Rekening_DB = data.getValue(3, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 10 Nama dan Bank Account/Textfield_Nama_Pemilik_Akun'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 10 Nama dan Bank Account/Textfield_Nama_Pemilik_Akun'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 10 Nama dan Bank Account/Btn_Nama_Bank'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 10 Nama dan Bank Account/Textfield_No_Rekening'), 0)

    //Type Nama Pemilik Akun
    Mobile.setText(findTestObject('First Loan/Step 10 Nama dan Bank Account/Textfield_Nama_Pemilik_Akun'), Nama_Pemilik_Akun_DB, 
        0)

    //Pilih Nama Bank
    Mobile.tap(findTestObject('First Loan/Step 10 Nama dan Bank Account/Btn_Nama_Bank'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 10 Nama dan Bank Account/List_Bank'), 0)

    Mobile.scrollToText(Nama_Bank_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 10 Nama dan Bank Account/Pilih_List_Bank', [('Pilih_NamaBank') : Nama_Bank_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 10 Nama dan Bank Account/Pilih_List_Bank', [('Pilih_NamaBank') : Nama_Bank_DB]), 
        0)

    //Type No Rekening Bank
    Mobile.setText(findTestObject('First Loan/Step 10 Nama dan Bank Account/Textfield_No_Rekening'), No_Rekening_DB, 0)

    Mobile.hideKeyboard()

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 10 Nama dan Bank Account/Btn_CheckBox_False'), 0)

   // boolean Checkbox_Unchecked = Mobile.verifyElementVisible(findTestObject('First Loan/Step 10 Nama dan Bank Account/Btn_CheckBox_False'), 
    //    0, FailureHandling.OPTIONAL)

    boolean Checkbox_Unchecked = Mobile.verifyElementAttributeValue(findTestObject('First Loan/Step 10 Nama dan Bank Account/Btn_CheckBox_False'), 'checked', 
        'false', 0, FailureHandling.OPTIONAL)

    if (Checkbox_Unchecked == true) {
        Mobile.tap(findTestObject('First Loan/Step 10 Nama dan Bank Account/Btn_Tap_CheckBox'), 0)
    } else {
        CustomKeywords.'set.testing.markPassed'('Box Already Checked')
    }
}

Mobile.tap(findTestObject('First Loan/Step 10 Nama dan Bank Account/Btn_Selanjutnya'), 0)

