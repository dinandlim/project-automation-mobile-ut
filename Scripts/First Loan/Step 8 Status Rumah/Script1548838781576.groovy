import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String text_StatusRumah

DBData data = findTestData('First Loan/Step 8 Status Rumah')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Status_Rumah_DB = data.getValue(1, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 8 Status Rumah/List_Rumah_OrangTua'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 8 Status Rumah/List_Rumah_OrangTua'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 8 Status Rumah/List_Kontrak'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 8 Status Rumah/List_Rumah_Sendiri'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 8 Status Rumah/List_Kos'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 8 Status Rumah/List_Rumah_Dinas'), 0)

    if (Status_Rumah_DB.equals('1')) {
        text_StatusRumah = 'Rumah Orang Tua'
    } else if (Status_Rumah_DB.equals('2')) {
        text_StatusRumah = 'Kontrak'
    } else if (Status_Rumah_DB.equals('3')) {
        text_StatusRumah = 'Rumah Sendiri'
    } else if (Status_Rumah_DB.equals('4')) {
        text_StatusRumah = 'Kos'
    } else if (Status_Rumah_DB.equals('5')) {
        text_StatusRumah = 'Rumah Dinas'
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Index not Valid.. Please Check DB')
    }
    
    Mobile.tap(findTestObject('First Loan/Step 8 Status Rumah/Pilih_List_StatusRumah', [('index_StatusRumah') : text_StatusRumah]), 
        0)
}

Mobile.tap(findTestObject('First Loan/Step 8 Status Rumah/Btn_Selanjutnya'), 0)

