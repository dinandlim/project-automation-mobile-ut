import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Boolean text_Pinjaman_Kilat = Mobile.verifyElementVisible(findTestObject('First Loan/PHA Page/Text_Mau_Pinjaman_Kilat'), 0, FailureHandling.OPTIONAL)

if (text_Pinjaman_Kilat == true) {
    Mobile.waitForElementPresent(findTestObject('First Loan/PHA Page/Btn_Tidak'), 0)

    Mobile.tap(findTestObject('First Loan/PHA Page/Btn_Tidak'), 0)

    WebUI.callTestCase(findTestCase('First Loan/Step 2 Alasan Pinjam'), [:], FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('First Loan/Step 2 Alasan Pinjam'), [:], FailureHandling.STOP_ON_FAILURE)
}

