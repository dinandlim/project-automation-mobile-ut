import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 9 Data Keluarga')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Keluarga_DB = data.getValue(1, index)

    NoTelp_Keluarga_DB = data.getValue(2, index)

    Alamat_Keluarga_DB = data.getValue(3, index)

    Provinsi_Keluarga_DB = data.getValue(4, index)

    Kota_Keluarga_DB = data.getValue(5, index)

    Kecamatan_Keluarga_DB = data.getValue(6, index)

    Kelurahan_Keluarga_DB = data.getValue(7, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Textfield_Nama_Keluarga'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 9 Data Keluarga/Textfield_Nama_Keluarga'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 9 Data Keluarga/Textfield_NoTelp_Keluarga'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 9 Data Keluarga/Textfield_Alamat_Keluarga'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Provinsi_Keluarga'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kota_Keluarga'), 0)

    Mobile.scrollToText('Kelurahan Tempat Tinggal Keluarga', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kecamatan_Keluarga'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kelurahan_Keluarga'), 0)

    Mobile.scrollToText('Nama Keluarga / Saudara', FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('First Loan/Step 9 Data Keluarga/Textfield_Nama_Keluarga'), Nama_Keluarga_DB, 0)

    Mobile.hideKeyboard()

    Mobile.setText(findTestObject('First Loan/Step 9 Data Keluarga/Textfield_NoTelp_Keluarga'), NoTelp_Keluarga_DB, 0)

    Mobile.hideKeyboard()

    Mobile.setText(findTestObject('First Loan/Step 9 Data Keluarga/Textfield_Alamat_Keluarga'), Alamat_Keluarga_DB, 0)

    Mobile.hideKeyboard()

    //Pilih Provinsi
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Provinsi_Keluarga'), 0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Provinsi_Keluarga'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/List_Provinsi_Keluarga'), 0)

    Mobile.scrollToText(Provinsi_Keluarga_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Provinsi_Keluarga', [('List_ProvinsiKeluarga') : Provinsi_Keluarga_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Provinsi_Keluarga', [('List_ProvinsiKeluarga') : Provinsi_Keluarga_DB]), 
        0)

    //Pilih Kota
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kota_Keluarga'), 0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kota_Keluarga'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/List_Kota_Keluarga'), 0)

    Mobile.scrollToText(Kota_Keluarga_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Kota_Keluarga', [('List_KotaKeluarga') : Kota_Keluarga_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Kota_Keluarga', [('List_KotaKeluarga') : Kota_Keluarga_DB]), 0)

    //Pilih Kecamatan
    Mobile.scrollToText('Kelurahan Tempat Tinggal Keluarga', FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kecamatan_Keluarga'), 0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kecamatan_Keluarga'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/List_Kecamatan_Keluarga'), 0)

    Mobile.scrollToText(Kecamatan_Keluarga_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Kecamatan_Keluarga', [('List_KecamatanKeluarga') : Kecamatan_Keluarga_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Kecamatan_Keluarga', [('List_KecamatanKeluarga') : Kecamatan_Keluarga_DB]), 
        0)

    //Pilih Kelurahan
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kelurahan_Keluarga'), 0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Kelurahan_Keluarga'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/List_Kelurahan_Keluarga'), 0)

    Mobile.scrollToText(Kelurahan_Keluarga_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Kelurahan_Keluarga', [('List_KelurahanKeluarga') : Kelurahan_Keluarga_DB]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Pilih_List_Kelurahan_Keluarga', [('List_KelurahanKeluarga') : Kelurahan_Keluarga_DB]), 
        0)
}

Mobile.waitForElementPresent(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Selanjutnya'), 0)

Mobile.tap(findTestObject('First Loan/Step 9 Data Keluarga/Btn_Selanjutnya'), 0)

