import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String text_JenisKelamin

DBData data = findTestData('First Loan/Step 3 Data Diri Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

Integer indexMonthHeader

Integer indexMonth

String textAgama

String textStatusPernikahan

for (def index : (1..data.getRowNumbers())) {
    Nama_Lengkap = data.getValue(1, index)

    Jenis_Kelamin = data.getValue(2, index)

    Tempat_Lahir = data.getValue(3, index)

    Tanggal_Lahir = data.getValue(4, index)

    Agama = data.getValue(5, index)

    Status_Pernikahan = data.getValue(6, index)

    String tanggal_lahir = Tanggal_Lahir

    String[] split = tanggal_lahir.split('-')

    String tanggal = split[0]

    String bulan = split[1]

    String tahun = split[2]

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Textfield_Nama_Lengkap'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Textfield_Nama_Lengkap'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Btn_Jenis_Kelamin'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Textfield_Tempat_Lahir'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Btn_Tanggal_Lahir'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Btn_Agama'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Btn_Status_Pernikahan'), 0)

    Mobile.setText(findTestObject('First Loan/Step 3 Data Diri/Textfield_Nama_Lengkap'), Nama_Lengkap, 0)

    //Pick Jenis Kelamin
    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Btn_Jenis_Kelamin'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Jenis Kelamin/List_Gender_Laki-laki'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Jenis Kelamin/List_Gender_Laki-laki'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Jenis Kelamin/List_Gender_Perempuan'), 0, FailureHandling.STOP_ON_FAILURE)

    if (Jenis_Kelamin.equals('M')) {
        text_JenisKelamin = 'Laki-laki'
    } else if (Jenis_Kelamin.equals('F')) {
        text_JenisKelamin = 'Perempuan'
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Pilihan Jenis Kelamin diluar index yang ditentukan. please check database')
    }
    
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Jenis Kelamin/List_Gender_Laki-laki'), 0)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Jenis Kelamin/Pilih_Gender', [('index_JenisKelamin') : text_JenisKelamin]), 
        0)

    //Write Tempat Lahir
    Mobile.setText(findTestObject('First Loan/Step 3 Data Diri/Textfield_Tempat_Lahir'), Tempat_Lahir, 0)

    Mobile.hideKeyboard()

    //Pick Tanggal Lahir
    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Btn_Tanggal_Lahir'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Datepicker_Year'), 0)

    //Pick Year
    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Datepicker_Year'), 0)

    Mobile.scrollToText(tahun, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Pilih_Tahun', [('index_PilihTahun') : tahun]), 0)

    //Pick Month
    HeaderDatePicker = Mobile.getText(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Text_Get_Month'), 0)

    StrHeaderDatePicker = HeaderDatePicker.replace(',', '')

    String[] SplitDatePicker = StrHeaderDatePicker.split(' ')

    String HeaderDate = SplitDatePicker[0]

    String HeaderMonth = SplitDatePicker[1]

    String HeaderYear = SplitDatePicker[2]

    if (HeaderMonth != bulan) {
        if (HeaderMonth.equals('Jan')) {
            indexMonthHeader = 1
        } else if (HeaderMonth.equals('Feb')) {
            indexMonthHeader = 2
        } else if (HeaderMonth.equals('Mar')) {
            indexMonthHeader = 3
        } else if (HeaderMonth.equals('Apr')) {
            indexMonthHeader = 4
        } else if (HeaderMonth.equals('May')) {
            indexMonthHeader = 5
        } else if (HeaderMonth.equals('Jun')) {
            indexMonthHeader = 6
        } else if (HeaderMonth.equals('Jul')) {
            indexMonthHeader = 7
        } else if (HeaderMonth.equals('Aug')) {
            indexMonthHeader = 8
        } else if (HeaderMonth.equals('Sep')) {
            indexMonthHeader = 9
        } else if (HeaderMonth.equals('Oct')) {
            indexMonthHeader = 10
        } else if (HeaderMonth.equals('Nov')) {
            indexMonthHeader = 11
        } else if (HeaderMonth.equals('Dec')) {
            indexMonthHeader = 12
        }
        
        if (bulan.equals('Jan')) {
            indexMonth = 1
        } else if (bulan.equals('Feb')) {
            indexMonth = 2
        } else if (bulan.equals('Mar')) {
            indexMonth = 3
        } else if (bulan.equals('Apr')) {
            indexMonth = 4
        } else if (bulan.equals('May')) {
            indexMonth = 5
        } else if (bulan.equals('Jun')) {
            indexMonth = 6
        } else if (bulan.equals('Jul')) {
            indexMonth = 7
        } else if (bulan.equals('Aug')) {
            indexMonth = 8
        } else if (bulan.equals('Sep')) {
            indexMonth = 9
        } else if (bulan.equals('Oct')) {
            indexMonth = 10
        } else if (bulan.equals('Nov')) {
            indexMonth = 11
        } else if (bulan.equals('Dec')) {
            indexMonth = 12
        }
    }
    
    if (indexMonthHeader < indexMonth) {
        for (int i = indexMonthHeader; i < indexMonth; i++) {
            Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Next'), 0)

            Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Next'), 0)
        }
    } else if (indexMonthHeader > indexMonth) {
        for (int i = indexMonthHeader; i > indexMonth; i--) {
            Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Previous'), 0)

            Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Previous'), 0)
        }
    } else if (indexMonthHeader == indexMonth) {
        CustomKeywords.'set.MarkAndMessage.markPassed'('Month Passed')
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Data not valid.. Please Check DB')
    }
    
    //Pick Date
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Tanggal', [('index_Tanggal_Lahir') : tanggal]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Tanggal', [('index_Tanggal_Lahir') : tanggal]), 0)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Tanggal Lahir/Btn_Ok'), 0)

    // Pick Agama
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Btn_Agama'), 0)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Btn_Agama'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Islam'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Islam'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Kristen_Protestan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Kristen_Katolik'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Buddha'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Hindu'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Konghucu'), 0)

    if (Agama.equals('1')) {
        textAgama = 'Islam'
    } else if (Agama.equals('2')) {
        textAgama = 'Kristen Protestan'
    } else if (Agama.equals('3')) {
        textAgama = 'Kristen Katolik'
    } else if (Agama.equals('4')) {
        textAgama = 'Buddha'
    } else if (Agama.equals('5')) {
        textAgama = 'Hindu'
    } else if (Agama.equals('6')) {
        textAgama = 'Konghucu'
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Index Value not valid.. Please Check DB')
    }
    
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Pilih_Agama', [('index_PilihAgama') : textAgama]), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Agama/List_Pilih_Agama', [('index_PilihAgama') : textAgama]), 0)

    //Status Pernikahan
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Btn_Status_Pernikahan'), 0)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Btn_Status_Pernikahan'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Lajang'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Lajang'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Duda'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Bercerai'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Menikah'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Janda'), 0)

    if (Status_Pernikahan.equals('1')) {
        textStatusPernikahan = 'Lajang'
    } else if (Status_Pernikahan.equals('2')) {
        textStatusPernikahan = 'Duda'
    } else if (Status_Pernikahan.equals('3')) {
        textStatusPernikahan = 'Bercerai'
    } else if (Status_Pernikahan.equals('4')) {
        textStatusPernikahan = 'Menikah'
    } else if (Status_Pernikahan.equals('5')) {
        textStatusPernikahan = 'Janda'
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Index not Valid.. Please Check DB')
    }
    
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Pilih_Status_Pernikahan', 
            [('index_Status_Pernikahan') : textStatusPernikahan]), 0)

    Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Status Pernikahan/List_Pilih_Status_Pernikahan', [('index_Status_Pernikahan') : textStatusPernikahan]), 
        0)
}

Mobile.tap(findTestObject('First Loan/Step 3 Data Diri/Btn_Selanjutnya'), 0)

