import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.Format as Format
import java.text.SimpleDateFormat as SimpleDateFormat
import java.util.Date as Date

String DurasiPinjaman = GlobalVariable.txt_durasi_pinjaman

String textBulan

Integer TextBulanDebitur

Integer TextBulanHrIni

Integer Usia


//System.out.println(TodayDate);
DBData data = findTestData('First Loan/Step Dokumen Persetujuan')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Debitur_DB = data.getValue(1, index)

    Tgl_Lahir_Debitur_DB = data.getValue(2, index)

    Tempat_Lahir_Debitur_DB = data.getValue(3, index)

    NoHp_Debitur_DB = data.getValue(4, index)

    TelpRumah_Debitur_DB = data.getValue(5, index)

    Email_Debitur_DB = data.getValue(6, index)

    Bank_Debitur_DB = data.getValue(7, index)

    Rekening_Debitur_DB = data.getValue(8, index)

    KTP_Debitur_DB = data.getValue(9, index)

    Alamat_Debitur_DB = data.getValue(10, index)
	
	Kota_Debitur_DB = data.getValue(11, index)
	
	Kecamatan_Debitur_DB = data.getValue(12, index)
	
	Kelurahan_Debitur_DB = data.getValue(13, index)
	
	Provinsi_Debitur_DB = data.getValue(14, index)
	
	
	//Deklarasi Verifikasi Tanggal Hari Ini
	Format formatter = new SimpleDateFormat('dd MMM yyyy')
	
	String TodayDate = formatter.format(new Date())
	
	
	//Deklarasi Verifikasi Alamat
	String Alamat_Debitur = Alamat_Debitur_DB +".\n" + Kota_Debitur_DB +".\n" + Kecamatan_Debitur_DB +".\n" + Kelurahan_Debitur_DB +".\n\n" + Provinsi_Debitur_DB +" - " + GlobalVariable.txt_kode_pos
	
	
	//Deklarasi Verifikasi Tanggal
	String tanggal_lahir = Tgl_Lahir_Debitur_DB
	
	String[] split = tanggal_lahir.split('-')
	
	String TanggalDB = split[0]
	
	String BulanDB = split[1]
	
	String TahunDB = split[2]
	
	if(BulanDB.equals("Jan")){
		textBulan = "1"
	}
	else if(BulanDB.equals("Feb")){
		textBulan = "2"
	}
	else if(BulanDB.equals("Mar")){
		textBulan = "3"
	}
	else if(BulanDB.equals("Apr")){
		textBulan = "4"
	}
	else if(BulanDB.equals("May")){
		textBulan = "5"
	}
	else if(BulanDB.equals("Jun")){
		textBulan = "6"
	}
	else if(BulanDB.equals("Jul")){
		textBulan = "7"
	}
	else if(BulanDB.equals("Aug")){
		textBulan = "8"
	}
	else if(BulanDB.equals("Sep")){
		textBulan = "9"
	}
	else if(BulanDB.equals("Oct")){
		textBulan = "10"
	}
	else if(BulanDB.equals("Nov")){
		textBulan = "11"
	}
	else if(BulanDB.equals("Dec")){
		textBulan = "12"
	}
	
	String Tgl_Lahir_Verfikasi = TanggalDB +"-" + textBulan +"-" + TahunDB

	//Deklarasi Verifikasi Usia Debitur
	//Tanggal Lahir Debitur
	String tanggal_lahir_Debitur = Tgl_Lahir_Debitur_DB
	
	String[] splitTglLahir = tanggal_lahir_Debitur.split('-')
	
	String TanggalDebitur = splitTglLahir[0]
	
	String BulanDebitur = splitTglLahir[1]
	
	String TahunDebitur = splitTglLahir[2]
	
	if(BulanDebitur.equals("Jan")){
		TextBulanDebitur = '1'
	}
	else if(BulanDebitur.equals("Feb")){
		TextBulanDebitur = '2'
	}
	else if(BulanDebitur.equals("Mar")){
		TextBulanDebitur = '3'
	}
	else if(BulanDebitur.equals("Apr")){
		TextBulanDebitur = '4'
	}
	else if(BulanDebitur.equals("May")){
		TextBulanDebitur = '5'
	}
	else if(BulanDebitur.equals("Jun")){
		TextBulanDebitur = '6'
	}
	else if(BulanDebitur.equals("Jul")){
		TextBulanDebitur = '7'
	}
	else if(BulanDebitur.equals("Aug")){
		TextBulanDebitur = '8'
	}
	else if(BulanDebitur.equals("Sep")){
		TextBulanDebitur = '9'
	}
	else if(BulanDebitur.equals("Oct")){
		TextBulanDebitur = '10'
	}
	else if(BulanDebitur.equals("Nov")){
		TextBulanDebitur = '11'
	}
	else if(BulanDebitur.equals("Dec")){
		TextBulanDebitur = '12'
	}
	
	//Tanggal Hari Ini
	//Format formatter = new SimpleDateFormat('dd MMM yyyy')
	
	//String TodayDate = formatter.format(new Date())
	
	String[] splitTodayDate = TodayDate.split(' ')
	
	String TanggalHrIni = splitTodayDate[0]
	
	String BulanHrIni = splitTodayDate[1]
	
	String TahunHrIni = splitTodayDate[2]
	
	if(BulanHrIni.equals("Jan")){
		TextBulanHrIni = '1'
	}
	else if(BulanHrIni.equals("Feb")){
		TextBulanHrIni = '2'
	}
	else if(BulanHrIni.equals("Mar")){
		TextBulanHrIni = '3'
	}
	else if(BulanHrIni.equals("Apr")){
		TextBulanHrIni = '4'
	}
	else if(BulanHrIni.equals("May")){
		TextBulanHrIni = '5'
	}
	else if(BulanHrIni.equals("Jun")){
		TextBulanHrIni = '6'
	}
	else if(BulanHrIni.equals("Jul")){
		TextBulanHrIni = '7'
	}
	else if(BulanHrIni.equals("Aug")){
		TextBulanHrIni = '8'
	}
	else if(BulanHrIni.equals("Sep")){
		TextBulanHrIni = '9'
	}
	else if(BulanHrIni.equals("Oct")){
		TextBulanHrIni = '10'
	}
	else if(BulanHrIni.equals("Nov")){
		TextBulanHrIni = '11'
	}
	else if(BulanHrIni.equals("Dec")){
		TextBulanHrIni = '12'
	}
	
	Integer intTanggalDebitur = Integer.parseInt(TanggalDB)
	
	Integer intTahunDebitur = Integer.parseInt(TahunDB)
	
	Integer intTanggalHrIni = Integer.parseInt(TanggalHrIni)
	
	Integer intTahunHrIni = Integer.parseInt(TahunHrIni)
	
	Integer TahunUmur = (intTahunHrIni - intTahunDebitur) - 1
	
	if(TextBulanHrIni > TextBulanDebitur){
		Usia = TahunUmur +1
	}
	else if(TextBulanHrIni == TextBulanDebitur){
		if(intTanggalHrIni >= intTanggalDebitur){
			Usia = TahunUmur + 1
		}
		else{
			Usia = TahunUmur
		}
	}
	else{
		Usia = TahunUmur
	}
	
	String VerifikasiUsia = Usia +" Tahun"
	
    //Rincian Pinjaman
    Mobile.waitForElementPresent(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Jumlah_Pembayaran'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Jumlah_Pembayaran'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Durasi_Pinjaman'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Tgl_Pengajuan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Biaya_Layanan'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Jatuh_Tempo'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Jumlah_Pembayaran'), 0)

    expected_JumlahPinjaman = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Jumlah_Pinjaman'), 
        0)

    expected_DurasiPinjaman = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Durasi_Pinjaman'), 
        0)

    expected_TglPengajuan = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Tgl_Pengajuan'), 
        0)

    expected_BiayaLayanan = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Biaya_Layanan'), 
        0)

    expected_JatuhTempo = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Jatuh_Tempo'), 0)

    expected_JumlahPembayaran = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Rincian Pinjaman/Text_Jumlah_Pembayaran'), 
        0)

    Mobile.verifyMatch(GlobalVariable.txt_jumlah_pinjaman, expected_JumlahPinjaman, false)

    DurasiPinjamanLow = DurasiPinjaman.toLowerCase()

    Mobile.verifyMatch(DurasiPinjamanLow, expected_DurasiPinjaman, false)

    Mobile.verifyMatch(TodayDate, expected_TglPengajuan, false)

    Mobile.verifyMatch(GlobalVariable.txt_biaya_layanan, expected_BiayaLayanan, false)

    Mobile.verifyMatch(GlobalVariable.txt_jatuh_tempo, expected_JatuhTempo, false)

    Mobile.verifyMatch(GlobalVariable.txt_jumlah_pembayaran, expected_JumlahPembayaran, false)

    //Data Debitur
    Mobile.scrollToText('Nama', FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Nama_Debitur'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Nama_Debitur'), 0)
	
	Mobile.scrollToText('Tgl. Lahir', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Tgl_Lahir_Debitur'), 0)
	
	Mobile.scrollToText('Tempat Lahir', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Tempat_Lahir_Debitur'), 0)
	
	Mobile.scrollToText('Usia', FailureHandling.STOP_ON_FAILURE)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Usia_Debitur'), 0)
	
	Mobile.scrollToText('No. Telp Handphone', FailureHandling.STOP_ON_FAILURE)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_Handphone_Debitur'), 0)
	
	Mobile.scrollToText('No. Telp Tempat Tinggal', FailureHandling.STOP_ON_FAILURE)
	
	Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_TelpRumah_Debitur'), 0)
	
	Mobile.scrollToText('Email', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Email_Debitur'), 0)
	
	Mobile.scrollToText('Bank', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Bank_Debitur'), 0)
	
	Mobile.scrollToText('No. Rekening', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_Rekening_Debitur'), 0)
	
	Mobile.scrollToText('No. KTP', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_KTP_Debitur'), 0)
	
	Mobile.scrollToText('Alamat', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Alamat_Debitur'), 0)
	
	Mobile.scrollToText('Nama', FailureHandling.STOP_ON_FAILURE)

    expected_NamaDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Nama_Debitur'), 0)
	
	Mobile.scrollToText('Tgl. Lahir', FailureHandling.STOP_ON_FAILURE)

    expected_TglLahirDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Tgl_Lahir_Debitur'), 
        0)
	
	Mobile.scrollToText('Tempat Lahir', FailureHandling.STOP_ON_FAILURE)

    expected_TempatLahirDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Tempat_Lahir_Debitur'), 
        0)
	
	Mobile.scrollToText('Usia', FailureHandling.STOP_ON_FAILURE)

    expected_UsiaDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Usia_Debitur'), 0)
	
	Mobile.scrollToText('No. Telp Handphone', FailureHandling.STOP_ON_FAILURE)
	
    expected_NoHpDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_Handphone_Debitur'), 
        0)
	
	Mobile.scrollToText('No. Telp Tempat Tinggal', FailureHandling.STOP_ON_FAILURE)

    expected_NoTelpDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_TelpRumah_Debitur'), 
        0)

	Mobile.scrollToText('Email', FailureHandling.STOP_ON_FAILURE)
	
    expected_EmailDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Email_Debitur'), 0)
	
	Mobile.scrollToText('Bank', FailureHandling.STOP_ON_FAILURE)

    expected_BankDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Bank_Debitur'), 0)
	
	Mobile.scrollToText('No. Rekening', FailureHandling.STOP_ON_FAILURE)

    expected_RekeningDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_Rekening_Debitur'), 
        0)
	
	Mobile.scrollToText('No. KTP', FailureHandling.STOP_ON_FAILURE)

    expected_KtpDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_No_KTP_Debitur'), 0)

	Mobile.scrollToText('Alamat', FailureHandling.STOP_ON_FAILURE)
	
    expected_AlamatDebitur = Mobile.getText(findTestObject('First Loan/Step Dokumen Persetujuan/Data Debitur/Text_Alamat_Debitur'), 
        0)
	
	Mobile.scrollToText('Nama', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(Nama_Debitur_DB, expected_NamaDebitur, false)
	
	Mobile.scrollToText('Tgl. Lahir', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(Tgl_Lahir_Verfikasi, expected_TglLahirDebitur, false)
	
	Mobile.scrollToText('Tempat Lahir', FailureHandling.STOP_ON_FAILURE)
	
    Mobile.verifyMatch(Tempat_Lahir_Debitur_DB, expected_TempatLahirDebitur, false)
	
	Mobile.scrollToText('Usia', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(VerifikasiUsia, expected_UsiaDebitur, false)
	
	Mobile.scrollToText('No. Telp Handphone', FailureHandling.STOP_ON_FAILURE)
	
    Mobile.verifyMatch(NoHp_Debitur_DB, expected_NoHpDebitur, false)
	
	Mobile.scrollToText('No. Telp Tempat Tinggal', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(TelpRumah_Debitur_DB, expected_NoTelpDebitur, false)
	
	Mobile.scrollToText('Email', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(Email_Debitur_DB, expected_EmailDebitur, false)
	
	Mobile.scrollToText('Bank', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(Bank_Debitur_DB, expected_BankDebitur, false)
	
	Mobile.scrollToText('No. Rekening', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(Rekening_Debitur_DB, expected_RekeningDebitur, false)
	
	Mobile.scrollToText('No. KTP', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(KTP_Debitur_DB, expected_KtpDebitur, false)
	
	Mobile.scrollToText('Alamat', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyMatch(Alamat_Debitur, expected_AlamatDebitur, false)
}

Mobile.tap(findTestObject('First Loan/Step Dokumen Persetujuan/Btn_Saya_Setuju'), 0)

