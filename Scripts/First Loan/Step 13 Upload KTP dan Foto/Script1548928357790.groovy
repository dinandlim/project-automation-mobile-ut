import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Selanjutnya'), 0)

Mobile.verifyElementVisible(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Upload_KTP'), 0)

Mobile.verifyElementVisible(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Upload_Foto'), 0)

Mobile.verifyElementVisible(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Lewati'), 0)

Mobile.verifyElementVisible(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Selanjutnya'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Upload_KTP'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Camera_Windows'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Shoot_Foto'), 0)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Done_Foto'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Done_Foto'), 0)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Validasi_Ok'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Validasi_Ok'), 0)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Upload_Foto'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Upload_Foto'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Camera_Windows'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Shoot_Foto'), 0)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Done_Foto'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Done_Foto'), 0)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Validasi_Ok'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Validasi_Ok'), 0)

Mobile.waitForElementPresent(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Selanjutnya'), 0)

Mobile.tap(findTestObject('First Loan/Step 13 Upload KTP dan Foto/Btn_Selanjutnya'), 0)

