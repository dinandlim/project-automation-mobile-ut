import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

boolean PersetujuanPinjaman = Mobile.verifyElementVisible(findTestObject('First Loan/Pinjaman Disetujui/Text Pinjaman Disetujui'), 
    0, FailureHandling.OPTIONAL)

if (PersetujuanPinjaman == true) {
    Mobile.waitForElementPresent(findTestObject('First Loan/Pinjaman Disetujui/Btn_Selanjutnya'), 0)

    Mobile.tap(findTestObject('First Loan/Pinjaman Disetujui/Btn_Selanjutnya'), 0)
} else {
    CustomKeywords.'set.testing.markFailedAndStop'('Pinjaman Tidak Disetujui')
}

