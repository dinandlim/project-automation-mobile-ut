import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String text_PendidikanTerakhir

DBData data = findTestData('First Loan/Step 4 Pendidikan Terakhir')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Pendidikan_Terakhir = data.getValue(1, index)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_SD'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_SD'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_SLTP'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_SLTA'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_DIPLOMA I'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_DIPLOMA II'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/LIst_DIPLOMA III'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_S1'), 0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_S2'), 0)

    Mobile.scrollToText('S3', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_S3'), 0)

    if (Pendidikan_Terakhir.equals('1')) {
        text_PendidikanTerakhir = 'SD'
    } else if (Pendidikan_Terakhir.equals('2')) {
        text_PendidikanTerakhir = 'SLTP'
    } else if (Pendidikan_Terakhir.equals('3')) {
        text_PendidikanTerakhir = 'SLTA'
    } else if (Pendidikan_Terakhir.equals('4')) {
        text_PendidikanTerakhir = 'DIPLOMA I'
    } else if (Pendidikan_Terakhir.equals('5')) {
        text_PendidikanTerakhir = 'DIPLOMA II'
    } else if (Pendidikan_Terakhir.equals('6')) {
        text_PendidikanTerakhir = 'DIPLOMA III'
    } else if (Pendidikan_Terakhir.equals('7')) {
        text_PendidikanTerakhir = 'S1'
    } else if (Pendidikan_Terakhir.equals('8')) {
        text_PendidikanTerakhir = 'S2'
    } else if (Pendidikan_Terakhir.equals('9')) {
        text_PendidikanTerakhir = 'S3'
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Index not Valid.. Please Check DB')
		
    }
    
    if (Pendidikan_Terakhir < 2) {
        Mobile.scrollToText(Pendidikan_Terakhir, FailureHandling.STOP_ON_FAILURE)
    }
    
    Mobile.tap(findTestObject('First Loan/Step 4 Pendidikan Terakhir/List_PilihPendidikan', [('index_PilihPendidikan') : text_PendidikanTerakhir]), 
        0)
}

Mobile.tap(findTestObject('First Loan/Step 4 Pendidikan Terakhir/Btn_Selanjutnya'), 0)

