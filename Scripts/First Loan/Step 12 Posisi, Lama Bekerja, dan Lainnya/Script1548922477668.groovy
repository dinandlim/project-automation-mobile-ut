import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Jenis_Pekerjaan_DB = data.getValue(1, index)

    Posisi_Jabatan_DB = data.getValue(2, index)

    Lama_Tahun_Bekerja_DB = data.getValue(3, index)

    Lama_Bulan_Bekerja_DB = data.getValue(4, index)

    Penghasilan_PerBulan_DB = data.getValue(5, index)

    Pengeluaran_PerBulan_DB = data.getValue(6, index)

    Angsuran_KPR_DB = data.getValue(7, index)

    Integer intPenghasilan_PerBulan_DB = Integer.parseInt(Penghasilan_PerBulan_DB)

    Integer intPengeluaran_PerBulan_DB = Integer.parseInt(Pengeluaran_PerBulan_DB)

    Integer intAngsuran_KPR_DB = Integer.parseInt(Angsuran_KPR_DB)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Jenis_Pekerjaan'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Jenis_Pekerjaan'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Posisi'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Tahun_Lama_Bekerja'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Bulan_Lama_Bekerja'), 
        0)

    Mobile.scrollToText('Angsuran KPR', FailureHandling.STOP_ON_FAILURE)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Penghasilan_perBulan'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Pengeluaran_perBulan'), 
        0)

    Mobile.verifyElementVisible(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Angsuran'), 
        0)

    Mobile.scrollToText('Jenis Pekerjaan', FailureHandling.STOP_ON_FAILURE)

    //Pilih Jenis Pekerjaan
    Mobile.tap(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Jenis_Pekerjaan'), 0)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/List_Jenis_Pekerjaan'), 
        0)

    Mobile.scrollToText(Jenis_Pekerjaan_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Pilih_List_Jenis_Pekerjaan', 
            [('Pilih_Jenis_Pekerjaan') : Jenis_Pekerjaan_DB]), 0)

    Mobile.tap(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Pilih_List_Jenis_Pekerjaan', [('Pilih_Jenis_Pekerjaan') : Jenis_Pekerjaan_DB]), 
        0)

    //Type Posisi
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Posisi'), 
        0)

    Mobile.setText(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Posisi'), Posisi_Jabatan_DB, 
        0)

    Mobile.hideKeyboard()

    //Pilih Lama Tahun Bekerja
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Tahun_Lama_Bekerja'), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Tahun_Lama_Bekerja'), 0)

    Mobile.scrollToText(Lama_Tahun_Bekerja_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Pilih_List_Lama_Tahun', 
            [('Lama_Tahun') : Lama_Tahun_Bekerja_DB]), 0)

    Mobile.tap(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Pilih_List_Lama_Tahun', [('Lama_Tahun') : Lama_Tahun_Bekerja_DB]), 
        0)

    //Pilih Lama Bulan Bekerja
    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Bulan_Lama_Bekerja'), 
        0)

    Mobile.tap(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Bulan_Lama_Bekerja'), 0)

    Mobile.scrollToText(Lama_Bulan_Bekerja_DB, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Pilih_List_Lama_Bulan', 
            [('Lama_Bulan') : Lama_Bulan_Bekerja_DB]), 0)

    Mobile.tap(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Pilih_List_Lama_Bulan', [('Lama_Bulan') : Lama_Bulan_Bekerja_DB]), 
        0)

    Mobile.scrollToText('Angsuran KPR', FailureHandling.STOP_ON_FAILURE)

    //Type Penghasilan PerBulan
    if ((intPenghasilan_PerBulan_DB < 100000000) && (intPenghasilan_PerBulan_DB >= 0)) {
        Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Penghasilan_perBulan'), 
            0)

        Mobile.setText(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Penghasilan_perBulan'), 
            Penghasilan_PerBulan_DB, 0)

        Mobile.hideKeyboard()
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Amount is not Valid.. Please check DB')
    }
    
    //Type Pengeluaran PerBulan
    if ((intPengeluaran_PerBulan_DB < 100000000) && (intPengeluaran_PerBulan_DB >= 0)) {
        Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Pengeluaran_perBulan'), 
            0)

        Mobile.setText(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Pengeluaran_perBulan'), 
            Pengeluaran_PerBulan_DB, 0)

        Mobile.hideKeyboard()
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Amount is not Valid.. Please check DB')
    }
    
    //Type Pengeluaran Angsuran KPR
    if ((intAngsuran_KPR_DB < 100000000) && (intAngsuran_KPR_DB >= 0)) {
        Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Angsuran'), 
            0)

        Mobile.setText(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Textfield_Angsuran'), Angsuran_KPR_DB, 
            0)

        Mobile.hideKeyboard()
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Amount is not Valid.. Please check DB')
    }
}

Mobile.waitForElementPresent(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Selanjutnya'), 0)

Mobile.tap(findTestObject('First Loan/Step 12 Posisi, Lama Bekerja, dan Lainnya/Btn_Selanjutnya'), 0)

