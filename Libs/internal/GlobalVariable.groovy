package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object txt_jumlah_pinjaman
     
    /**
     * <p></p>
     */
    public static Object txt_durasi_pinjaman
     
    /**
     * <p></p>
     */
    public static Object txt_biaya_layanan
     
    /**
     * <p></p>
     */
    public static Object txt_jumlah_pembayaran
     
    /**
     * <p></p>
     */
    public static Object txt_jatuh_tempo
     
    /**
     * <p></p>
     */
    public static Object txt_kode_pos
     
    /**
     * <p></p>
     */
    public static Object txt_loan_id
     
    /**
     * <p></p>
     */
    public static Object txt_executor
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['txt_jumlah_pinjaman' : '', 'txt_durasi_pinjaman' : '', 'txt_biaya_layanan' : '', 'txt_jumlah_pembayaran' : '', 'txt_jatuh_tempo' : '', 'txt_kode_pos' : '', 'txt_loan_id' : ''])
        allVariables.put('Dora', allVariables['default'] + ['txt_executor' : 'Dora'])
        allVariables.put('Ferdinand', allVariables['default'] + ['txt_executor' : 'Ferdinand'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        def selectedVariables = allVariables[profileName]
		
		for(object in selectedVariables){
			String overridingGlobalVariable = RunConfiguration.getOverridingGlobalVariable(object.key)
			if(overridingGlobalVariable != null){
				selectedVariables.put(object.key, overridingGlobalVariable)
			}
		}

        txt_jumlah_pinjaman = selectedVariables["txt_jumlah_pinjaman"]
        txt_durasi_pinjaman = selectedVariables["txt_durasi_pinjaman"]
        txt_biaya_layanan = selectedVariables["txt_biaya_layanan"]
        txt_jumlah_pembayaran = selectedVariables["txt_jumlah_pembayaran"]
        txt_jatuh_tempo = selectedVariables["txt_jatuh_tempo"]
        txt_kode_pos = selectedVariables["txt_kode_pos"]
        txt_loan_id = selectedVariables["txt_loan_id"]
        txt_executor = selectedVariables["txt_executor"]
        
    }
}
